-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2019 at 11:45 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ckt`
--

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE `goods` (
  `goods_id` int(11) NOT NULL,
  `receive_date` date NOT NULL,
  `ordered_from` text NOT NULL,
  `article_code` int(14) NOT NULL,
  `subclass` text NOT NULL,
  `external_code` int(8) NOT NULL,
  `article_description` text NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double NOT NULL,
  `total_price` double NOT NULL,
  `status` text NOT NULL,
  `warehouse` text NOT NULL,
  `rack` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `order_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `ordered_from` text NOT NULL,
  `delivered_to` text NOT NULL,
  `article_code` int(14) NOT NULL,
  `subclass` text NOT NULL,
  `external_code` int(8) NOT NULL,
  `article_description` text NOT NULL,
  `qty_order` int(11) NOT NULL,
  `price` double NOT NULL,
  `total_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`order_id`, `order_no`, `order_date`, `ordered_from`, `delivered_to`, `article_code`, `subclass`, `external_code`, `article_description`, `qty_order`, `price`, `total_price`) VALUES
(1, 123, '2019-07-12', 'DC', 'KPT', 12321321, 'LA1', 111133, '123123', 3, 12000, 36000),
(3, 123, '2019-07-12', 'DC', 'KPT', 12321321, 'LA1', 111133, '123123', 3, 12000, 36000),
(4, 31299, '2019-07-13', 'DC', 'KPT', 23456618, 'LA2', 111134, 'Toys', 2, 12000, 24000),
(6, 123, '2019-07-12', 'DC', 'KPT', 12321321, 'LA1', 111133, '123123', 3, 12000, 36000),
(7, 31299, '2019-07-13', 'DC', 'KPT', 23456618, 'LA2', 111134, 'Toys', 2, 12000, 24000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `divisi` varchar(50) NOT NULL,
  `departement` varchar(50) NOT NULL,
  `cabang` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `jabatan`, `divisi`, `departement`, `cabang`) VALUES
(2, 'aldi', '5cf15fc7e77e85f5d525727358c0ffc9', 'aldi muhammad iqbal', 'Super Admin', 'Disposable', 'GMS', 'YGC'),
(4, 'spv', 'f4984324c6673ce07aafac15600af26e', 'supervisor', 'Supervisor', '', '', 'KPT'),
(5, 'pg', '235ec52392b77977539cf78b62e708d3', 'PG', 'PG', '', '', ''),
(6, 'receiving', '0639f5c0e2228ccdf3385f88f1579491', 'Receiving', 'Receiving', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`goods_id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `goods`
--
ALTER TABLE `goods`
  MODIFY `goods_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
