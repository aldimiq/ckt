<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url().'assets/img/web_icon.png'?>">
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/img/web_icon.png'?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    CKT
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet" />
  <link href="<?php echo base_url().'assets/css/now-ui-kit.css?v=1.3.0'?>" rel="stylesheet" />
  <!-- Datatables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>

<body class="landing-page sidebar-collapse">
  <?php $this->load->view('/Pages/navbar');?> <!--Include Navbar-->
  <div class="wrapper">
    <div class="page-header page-header-small">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url().'assets/img/bg6.jpg'?>');">
      </div>
      <div class="content-center">
        <div class="container">
          <h1 class="title">This is our great company.</h1>
        </div>
      </div>
    </div>
    <div class="section ">
      <div class="container">
        <h2 class="title text-center">User Management</h2>
        <div class="btn-group">
          <button class="btn btn-success" data-toggle="modal" data-target="#myModal" onclick="add_user()"><i class="fa fa-plus-circle"></i>
            Tambah User</button>
          <button class="btn btn-default" onclick="reload_table()"><i class="fa fa-refresh fa-spin"></i> Reload</button>
        </div>
        <table id="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Jabatan</th>
              <th>Departement</th>
              <th>Divisi</th>
              <th style="width:130px;">Action</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Jabatan</th>
              <th>Departement</th>
              <th>Divisi</th>
              <th style="width:130px;">Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <?php $this->load->view('/Pages/footer');?> <!--Include Footer-->
  </div>
  <?php $this->load->view('/User_Management/modal');?> <!--Include Modal-->

  <!--   Core JS Files   -->
  <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url().'assets/js/plugins/bootstrap-switch.js'?>"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url().'assets/js/now-ui-kit.js?v=1.3.0'?>" type="text/javascript"></script>
  <!-- Datatables -->
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
  <script type="text/javascript">

    var save_method; //for save method string
    var table;

    $(document).ready(function () {

      //datatables
      table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo site_url('user_management/ajax_list')?>",
          "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          {
            "targets": [-1], //last column
            "orderable": false, //set not orderable
          },
        ],

      });

      //set input/textarea/select event when change value, remove class error and remove text help user_management 
      $("input").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
      });
      $("textarea").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
      });
      $("select").change(function () {
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
      });

    });



    function add_user() {
      save_method = 'add';
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-user_management').empty(); // clear error string
      $('#modal_form').modal('show'); // show bootstrap modal
      $('.modal-title').text('Tambah User'); // Set Title to Bootstrap modal title
    }

    function edit_user(id) {
      save_method = 'update';
      console.log('update:' + id);
      $('#form')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-user_management').empty(); // clear error string

      //Ajax Load data from ajax
      $.ajax({
        url: "<?php echo site_url('user_management/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {

          $('[name="id"]').val(data.id);
          $('[name="username"]').val(data.username);
          $('[name="nama"]').val(data.nama);
          $('[name="jabatan"]').val(data.jabatan);
          $('[name="divisi"]').val(data.divisi);
          $('[name="departement"]').val(data.departement);
          $('[name="cabang"]').val(data.cabang);
          $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Edit User'); // Set title to Bootstrap modal title
        },
        error: function (jqXHR, textStatus, errorThrown) {
          alert('Error get data from ajax');
        }
      });
    }

    function reload_table() {
      table.ajax.reload(null, false); //reload datatable ajax 
    }

    function save() {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled', true); //set button disable 
      var url;

      if (save_method == 'add') {
        url = "<?php echo site_url('user_management/ajax_add')?>";
      } else {
        url = "<?php echo site_url('user_management/ajax_update')?>";
      }
      console.log(url);
      // ajax adding data to database
      $.ajax({
        url: url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function (data) {

          if (data.status) //if success close modal and reload ajax table
          {
            $('#modal_form').modal('hide');
            reload_table();
          }
          else {
            for (var i = 0; i < data.inputerror.length; i++) {
              $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
              $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]); //select span help-user_management class set text error string
            }
          }
          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled', false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown) {
          alert('Error adding / update data');
          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled', false); //set button enable 

        }
      });
    }

    function delete_user(id) {
      if (confirm('Are you sure delete this data?')) {
        // ajax delete data to database
        $.ajax({
          url: "<?php echo site_url('user_management/ajax_delete')?>/" + id,
          type: "POST",
          dataType: "JSON",
          success: function (data) {
            //if success reload ajax table
            $('#modal_form').modal('hide');
            reload_table();
          },
          error: function (jqXHR, textStatus, errorThrown) {
            alert('Error deleting data');
          }
        });

      }
    }
  </script>
</body>
</html>