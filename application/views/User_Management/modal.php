<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Title modal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
        <form class="form" method="post" action="#" id="form">
          <div class="form-row">
            <input type="text" class="form-control" name ="id" id="id" hidden>
            <div class="form-group col-md-6">
              <label>Username</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Username">
              <span class="help-user_management"></span>
            </div>
            <div class="form-group col-md-6">
              <label >Password</label>
              <input type="password" class="form-control" id="Password" name="password" placeholder="Password">
              <span class="help-user_management"></span>
            </div>
          </div>
          <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Full Name">
            <span class="help-user_management"></span>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label >Jabatan</label>
              <select id="jabatan" name="jabatan" class="form-control">
                <option value="Super Admin">Super Admin</option> 
                <option value="Receiving">Receiving</option> 
                <option value="Supervisor">Supervisor</option>
                <option value="Manager Department">Manager Department</option>
                <option value="Manager Toko">Manager Toko</option>
                <option value="PG">PG</option>
              </select>
              <span class="help-user_management"></span>
            </div>
            <div class="form-group col-md-6">
              <label >Departement</label>
              <input type="text" class="form-control" id="departement" name="departement">
              <span class="help-user_management"></span>
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-8">
              <label >Divisi</label>
              <input type="text" class="form-control" id="divisi" name="divisi">
              <span class="help-user_management"></span>
            </div>
            <div class="form-group col-md-4">
              <label >Cabang (Kode)</label>
              <input type="text" class="form-control" id="cabang" name="cabang">
              <span class="help-user_management"></span>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>