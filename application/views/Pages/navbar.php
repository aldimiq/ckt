<nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
  <div class="container">
    <div class="dropdown button-dropdown">
      <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
        <span class="button-bar"></span>
        <span class="button-bar"></span>
        <span class="button-bar"></span>
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-header">Menu</a>
        <!-- Menu for Super Admin -->
        <?php if($this->session->userdata('ses_jabatan') == "Super Admin"){?>
        <a class="dropdown-item" href="<?php echo base_url("index.php/User_Management"); ?>">User Management</a>
        <a class="dropdown-item" href="<?php echo base_url("index.php/Purchase_Order"); ?>">Purchase Order</a>
        <!-- Menu for Supervisor -->
        <?php }elseif($this->session->userdata('ses_jabatan') == "Supervisor"){ ?>
        <a class="dropdown-item" href="<?php echo base_url("index.php/Purchase_Order"); ?>">Purchase Order</a>
        <?php } ?>
      </div>
    </div>
    <div class="navbar-translate">
      <b>CKT</b>
    </div>
    <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="<?php echo base_url().'assets/img/blurred-image-1.jpg'?>">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="<?php echo base_url().'index.php/auth/logout'?>" ><b>Sign out</b></a>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>