<footer class="footer footer-default">
  <div class=" container ">
    <div class="copyright" id="copyright">
      &copy;
      <script>
        document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
      </script>, Designed by
      Team
    </div>
  </div>
</footer>
<?php $this->load->view('Pages/alert'); ?><!--Include Alert -->