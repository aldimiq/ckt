<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url().'assets/img/web_icon.png'?>">
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/img/web_icon.png'?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    CKT
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet" />
  <link href="<?php echo base_url().'assets/css/now-ui-kit.css?v=1.3.0'?>" rel="stylesheet" />
  <!-- Datatables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  <script>
  $(document).ready(function(){
    // Sembunyikan alert validasi kosong
    $("#kosong").hide();
  });
  </script>
</head>

<body class="landing-page sidebar-collapse">
  <?php $this->load->view('/Pages/navbar');?> <!--Include Navbar-->
  <div class="wrapper">
    <div class="page-header page-header-small">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url().'assets/img/bg6.jpg'?>');">
      </div>
      <div class="content-center">
        <div class="container">
          <h1 class="title">This is our great company.</h1>
        </div>
      </div>
    </div>
    <div class="section ">
      <div class="container">
        <h2 class="title text-center">Import Data Purchase Order</h2>
        <form method="post" action="<?php echo base_url("index.php/Purchase_Order/form"); ?>" enctype="multipart/form-data">
          <!-- 
          -- Buat sebuah input type file
          -- class pull-left berfungsi agar file input berada di sebelah kiri
          -->
          <input type="file" name="file">
          
          <!--
          -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
          -->
          <input type="submit" name="preview" value="Preview">
        </form>
        <a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a><br>
        <?php
        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
          if(isset($upload_error)){ // Jika proses upload gagal
            echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
            die; // stop skrip
          }
          
          // Buat sebuah tag form untuk proses import data ke database
          echo "<form method='post' action='".base_url("index.php/Purchase_Order/import")."'>";
          
          // Buat sebuah div untuk alert validasi kosong
          echo "<div style='color: red;' id='kosong'>
          Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
          </div>";
          
          echo "<table border='1' cellpadding='8'>
          <tr>
            <th colspan='11'>Preview Data</th>
          </tr>
          <tr>
            <th>Order No</th>
            <th>Order Date</th>
            <th>Ordered From</th>
            <th>Delivered To</th>
            <th>Article Code</th>
            <th>SubClass</th>
            <th>External Code</th>
            <th>Article Description</th>
            <th>Qty Order</th>
            <th>Price</th>
            <th>Total Price</th>
          </tr>";
          
          $numrow = 1;
          $kosong = 0;
          
          // Lakukan perulangan dari data yang ada di excel
          // $sheet adalah variabel yang dikirim dari controller
          foreach($sheet as $row){ 
            // Ambil data pada excel sesuai Kolom
            $order_no=$row['A'];
            $order_date=$row['B'];
            $ordered_from=$row['C'];
            $delivered_to=$row['D'];
            $article_code=$row['E'];
            $subclass=$row['F'];
            $external_code=$row['G'];
            $article_description=$row['H'];
            $qty_order=$row['I'];
            $price=$row['J'];
            $total_price=$row['K'];
            
            // Cek jika semua data tidak diisi
            if(empty($order_no) && 
              empty($order_date) && 
              empty($ordered_from) && 
              empty($delivered_to) &&
              empty($article_code) &&
              empty($subclass) &&
              empty($external_code) &&
              empty($article_description) &&
              empty($qty_order) &&
              empty($price) &&
              empty($total_price)
              )
              continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
            
            // Cek $numrow apakah lebih dari 1
            // Artinya karena baris pertama adalah nama-nama kolom
            // Jadi dilewat saja, tidak usah diimport
            if($numrow > 1){
              // Validasi apakah semua data telah diisi
              $order_no_td = ( ! empty($order_no))? "" : " style='background: #E07171;'";
              $order_date_td = ( ! empty($order_date))? "" : " style='background: #E07171;'";
              $ordered_from_td = ( ! empty($ordered_from))? "" : " style='background: #E07171;'";
              $delivered_to_td = ( ! empty($delivered_to))? "" : " style='background: #E07171;'";
              $article_code_td = ( ! empty($article_code))? "" : " style='background: #E07171;'";
              $subclass_td = ( ! empty($subclass))? "" : " style='background: #E07171;'";
              $external_code_td = ( ! empty($external_code))? "" : " style='background: #E07171;'";
              $article_description_td = ( ! empty($article_description))? "" : " style='background: #E07171;'";
              $qty_order_td = ( ! empty($qty_order))? "" : " style='background: #E07171;'";
              $price_td = ( ! empty($price))? "" : " style='background: #E07171;'";
              $total_price_td = ( ! empty($total_price))? "" : " style='background: #E07171;'";
              // Jika salah satu data ada yang kosong
              if(empty($order_no) && 
              empty($order_date) && 
              empty($ordered_from) && 
              empty($delivered_to) &&
              empty($article_code) &&
              empty($subclass) &&
              empty($external_code) &&
              empty($article_description) &&
              empty($qty_order) &&
              empty($price) &&
              empty($total_price)
              ){
                $kosong++; // Tambah 1 variabel $kosong
              }
              
              echo "<tr>";
              echo "<td".$order_no_td.">".$order_no."</td>";
              echo "<td".$order_date_td.">".$order_date."</td>";
              echo "<td".$ordered_from_td.">".$ordered_from."</td>";
              echo "<td".$delivered_to_td.">".$delivered_to."</td>";
              echo "<td".$article_code_td.">".$article_code."</td>";
              echo "<td".$subclass_td.">".$subclass."</td>";
              echo "<td".$external_code_td.">".$external_code."</td>";
              echo "<td".$article_description_td.">".$article_description."</td>";
              echo "<td".$qty_order_td.">".$qty_order."</td>";
              echo "<td".$price_td.">".$price."</td>";
              echo "<td".$total_price_td.">".$total_price."</td>";
              echo "</tr>";
            }
            
            $numrow++; // Tambah 1 setiap kali looping
          }
          
          echo "</table>";
          
          // Cek apakah variabel kosong lebih dari 0
          // Jika lebih dari 0, berarti ada data yang masih kosong
          if($kosong > 0){
          ?>  
            <script>
            $(document).ready(function(){
              // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
              $("#jumlah_kosong").html('<?php echo $kosong; ?>');
              
              $("#kosong").show(); // Munculkan alert validasi kosong
            });
            </script>
          <?php
          }else{ // Jika semua data sudah diisi
            echo "<hr>";
            
            // Buat sebuah tombol untuk mengimport data ke database
            echo "<button type='submit' name='import'>Import</button>";
            echo "<a href='".base_url("index.php/Purchase_Order")."'>Cancel</a>";
          }
          
          echo "</form>";
        }
        ?>
      </div>
    </div>
    <?php $this->load->view('/Pages/footer');?> <!--Include Footer-->
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url().'assets/js/plugins/bootstrap-switch.js'?>"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url().'assets/js/now-ui-kit.js?v=1.3.0'?>" type="text/javascript"></script>
  <!-- Datatables -->
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

</body>
</html>