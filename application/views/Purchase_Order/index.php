<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url().'assets/img/web_icon.png'?>">
  <link rel="icon" type="image/png" href="<?php echo base_url().'assets/img/web_icon.png'?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    CKT
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
  <!-- CSS Files -->
  <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet" />
  <link href="<?php echo base_url().'assets/css/now-ui-kit.css?v=1.3.0'?>" rel="stylesheet" />
  <!-- Datatables -->
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
</head>

<body class="landing-page sidebar-collapse">
  <?php $this->load->view('/Pages/navbar');?> <!--Include Navbar-->
  <div class="wrapper">
    <div class="page-header page-header-small">
      <div class="page-header-image" data-parallax="true" style="background-image: url('<?php echo base_url().'assets/img/bg6.jpg'?>');">
      </div>
      <div class="content-center">
        <div class="container">
          <h1 class="title">This is our great company.</h1>
        </div>
      </div>
    </div>
    <div class="section ">
      <div class="container">
        <h2 class="title text-center">Purchase Order</h2>
        <?php if($this->session->userdata('ses_jabatan') == "Supervisor"){?>
          <form method="" action="<?php echo base_url("index.php/Purchase_Order/form"); ?>">
            <button class="btn btn-success" id="submit-buttons" type="submit" ​​​​​><i class="fa fa-plus-circle"></i>Import Data</button>
          </form><br>
        <?php }elseif($this->session->userdata('ses_jabatan') == "Super Admin"){ ?>
          <form method="" action="<?php echo base_url("index.php/Purchase_Order/form"); ?>">
            <button class="btn btn-success" id="submit-buttons" type="submit" ​​​​​><i class="fa fa-plus-circle"></i>Import Data</button>
          </form><br>
        <?php }elseif($this->session->userdata('ses_jabatan') == "Receiving"){ ?>
        <?php } ?>
        <table id="table">
          <thead>
            <tr>
              <th>Order No</th>
              <th>Order Date</th>
              <th>Ordered From</th>
              <th>Delivered To</th>
              <th>Article Code</th>
              <th>External Code</th>
              <th>Article Description</th>
              <th>Qty Order</th>
              <?php if($this->session->userdata('ses_jabatan') == "Super Admin"){?>
                <th>Action</th>
              <?php }elseif($this->session->userdata('ses_jabatan') == "Supervisor"){ ?>
                <th>Action</th>
              <?php }elseif($this->session->userdata('ses_jabatan') == "Receiving"){?>
              <?php ?>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th>Order No</th>
              <th>Order Date</th>
              <th>Ordered From</th>
              <th>Delivered To</th>
              <th>Article Code</th>
              <th>External Code</th>
              <th>Article Description</th>
              <th>Qty Order</th>
              <?php if($this->session->userdata('ses_jabatan') == "Super Admin"){?>
                <th>Action</th>
              <?php }elseif($this->session->userdata('ses_jabatan') == "Supervisor"){ ?>
                <th>Action</th>
              <?php }elseif($this->session->userdata('ses_jabatan') == "Receiving"){?>
              <?php }?>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <?php $this->load->view('/Pages/footer');?> <!--Include Footer-->
  </div>
  <!--   Core JS Files   -->
  <script src="<?php echo base_url().'assets/js/core/jquery.min.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/js/core/popper.min.js'?>" type="text/javascript"></script>
  <script src="<?php echo base_url().'assets/js/core/bootstrap.min.js'?>" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="<?php echo base_url().'assets/js/plugins/bootstrap-switch.js'?>"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url().'assets/js/now-ui-kit.js?v=1.3.0'?>" type="text/javascript"></script>
  <!-- Datatables -->
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
  <script type="text/javascript">

    var table;

    $(document).ready(function () {

      //datatables
      table = $('#table').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        // Load data for the table's content from an Ajax source
        "ajax": {
          "url": "<?php echo site_url('purchase_order/ajax_list')?>",
          "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
          {
            "targets": [-1], //last column
            "orderable": false, //set not orderable
          },
        ],

      });

    });


    function reload_table() {
      table.ajax.reload(null, false); //reload datatable ajax 
    }

    function delete_order(order_id) {
      if (confirm('Are you sure delete this data?')) {
        // ajax delete data to database
        $.ajax({
          url: "<?php echo site_url('purchase_order/ajax_delete')?>/" + order_id,
          type: "POST",
          dataType: "JSON",
          success: function (data) {
            //if success reload ajax table
            $('#modal_form').modal('hide');
            reload_table();
          },
          error: function (jqXHR, textStatus, errorThrown) {
            alert('Error deleting data');
          }
        });

      }
    }
  </script>
</body>
</html>