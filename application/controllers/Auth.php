<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('Auth_model','auth');
		}

		// Login Function
		function auth(){
			$username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
	    	$password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);
	    	$cek_user=$this->auth->auth_user($username,$password);
			if ($cek_user->num_rows() > 0) {
				$data=$cek_user->row_array();
		      	$this->session->set_userdata('masuk',TRUE);
	      		if ($data['jabatan']=='Super Admin') {
		        $this->session->set_userdata('ses_id',$data['id']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				$this->session->set_userdata('ses_jabatan',$data['jabatan']);
		        $this->session->set_flashdata('success', 'Login Success !');
		        redirect('Super_Admin');
		      	}elseif ($data['jabatan']=='Receiving') {
		        $this->session->set_userdata('ses_id',$data['id']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				$this->session->set_userdata('ses_jabatan',$data['jabatan']);
		        $this->session->set_flashdata('success', 'Login Success !');
		        redirect('Receiving');
	  			}elseif ($data['jabatan']=='Supervisor') {
		        $this->session->set_userdata('ses_id',$data['id']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				$this->session->set_userdata('ses_jabatan',$data['jabatan']);
		        $this->session->set_flashdata('success', 'Login Success !');
		        redirect('Supervisor');
	  			}elseif ($data['jabatan']=='Manager Department') {
		        $this->session->set_userdata('ses_id',$data['id']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				$this->session->set_userdata('ses_jabatan',$data['jabatan']);
		        $this->session->set_flashdata('success', 'Login Success !');
		        redirect('Manager Department');
	  			}elseif ($data['jabatan']=='Manager Toko') {
		        $this->session->set_userdata('ses_id',$data['id']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				$this->session->set_userdata('ses_jabatan',$data['jabatan']);
		        $this->session->set_flashdata('success', 'Login Success !');
		        redirect('Manager Toko');
	  			}elseif ($data['jabatan']=='PG') {
		        $this->session->set_userdata('ses_id',$data['id']);
				$this->session->set_userdata('ses_nama',$data['nama']);
				$this->session->set_userdata('ses_jabatan',$data['jabatan']);
		        $this->session->set_flashdata('success', 'Login Success !');
		        redirect('PG');
	  			}
		    }else{
		      $url=base_url();
		      $this->session->set_flashdata('warning','Username Or Password Wrong');
		      redirect($url);
		    }//endif
		}//end funtion auth

		// Logout function
		function logout(){
			$this->session->sess_destroy();
	        $url=base_url('');
	        redirect($url);
		}
	}
?>