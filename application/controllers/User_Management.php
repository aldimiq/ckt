<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Management extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('User_Management_model','user_management');
    }

	public function index(){
		$this->load->view('User_Management/index');
	}
	public function ajax_list(){
        $list = $this->user_management->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $user_management) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $user_management->nama;
            $row[] = $user_management->username;
            $row[] = $user_management->jabatan;
	        $row[] = $user_management->departement;
            $row[] = $user_management->divisi;
            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_user('."'".$user_management->id."'".')"><i class="fa fa-edit"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_user('."'".$user_management->id."'".')"><i class="fa fa-remove"></i> Delete</a>';
         
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->user_management->count_all(),
                        "recordsFiltered" => $this->user_management->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id){
        $data = $this->user_management->get_by_id($id);
        echo json_encode($data);
    }


    public function ajax_add(){
        $this->_validate();
        $data = array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'nama'=>$this->input->post('nama'),
                'jabatan'=>$this->input->post('jabatan'),
                'divisi'=>$this->input->post('divisi'),
	            'departement'=>$this->input->post('departement'),
                'cabang'=>$this->input->post('cabang')
            );
        $insert = $this->user_management->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_update(){
        $this->_validate();
        $data = array(
                'username'=>$this->input->post('username'),
                'password'=>md5($this->input->post('password')),
                'nama'=>$this->input->post('nama'),
                'jabatan'=>$this->input->post('jabatan'),
                'divisi'=>$this->input->post('divisi'),
	            'departement'=>$this->input->post('departement'),
                'cabang'=>$this->input->post('cabang')
            );
        $this->user_management->update(array('id' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_delete($id){
        $this->user_management->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('username') == '')
        {
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Username is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('password') == '')
        {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'Password is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('nama') == '')
        {
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('jabatan') == '')
        {
            $data['inputerror'][] = 'jabatan';
            $data['error_string'][] = 'Jabatan is required';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}
