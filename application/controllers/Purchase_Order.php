<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_Order extends CI_Controller {
  private $filename = "import_data";
	public function __construct(){
        parent::__construct();
        $this->load->model('Purchase_Order_model','purchase_order');
        if($this->session->userdata('masuk') != TRUE){
          $url=base_url();
          redirect($url);
      }
    }

	public function index(){
		$this->load->view('Purchase_Order/index');
	}
	public function ajax_list(){
        $list = $this->purchase_order->get_datatables();
        $data = array();
        foreach ($list as $purchase_order) {    
            $row = array();
            $row[] = $purchase_order->order_no;
            $row[] = $purchase_order->order_date;
            $row[] = $purchase_order->ordered_from;
	          $row[] = $purchase_order->delivered_to;
            $row[] = $purchase_order->article_code;
            $row[] = $purchase_order->external_code;
            $row[] = $purchase_order->article_description;
            $row[] = $purchase_order->qty_order;

            if($this->session->userdata('ses_jabatan') == "Super Admin"){
            $row[] = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_order('."'".$purchase_order->order_id."'".')"><i class="fa fa-remove"></i> Delete</a>';
            }elseif($this->session->userdata('ses_jabatan') == "Supervisor"){
            $row[] = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_order('."'".$purchase_order->order_id."'".')"><i class="fa fa-remove"></i> Delete</a>';
            }elseif($this->session->userdata('ses_jabatan') == "Receiving"){}
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->purchase_order->count_all(),
                        "recordsFiltered" => $this->purchase_order->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function form(){
        $data = array(); // Buat variabel $data sebagai array
        
        if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
            // lakukan upload file dengan memanggil function upload yang ada di Purchase_Order_Model.php
            $upload = $this->purchase_order->upload_file($this->filename);
            
            if($upload['result'] == "success"){ // Jika proses upload sukses
            // Load plugin PHPExcel nya
            include APPPATH.'third_party/PHPExcel/PHPExcel.php';
            
            $excelreader = new PHPExcel_Reader_Excel2007();
            $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
            $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
            
            // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
            // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
            $data['sheet'] = $sheet; 
            }else{ // Jika proses upload gagal
            $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
            }
        }
    $this->load->view('Purchase_Order/form', $data);
    }    

    public function import(){
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
        
        // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
        $data = array();
        
        $numrow = 1;
        foreach($sheet as $row){
          // Cek $numrow apakah lebih dari 1
          // Artinya karena baris pertama adalah nama-nama kolom
          // Jadi dilewat saja, tidak usah diimport
          if($numrow > 1){
            // Kita push (add) array data ke variabel data
            array_push($data, array(
              'order_no'=>$row['A'],
              'order_date'=>$row['B'],
              'ordered_from'=>$row['C'],
              'delivered_to'=>$row['D'],
              'article_code'=>$row['E'],
              'subclass'=>$row['F'],
              'external_code'=>$row['G'],
              'article_description'=>$row['H'],
              'qty_order'=>$row['I'],
              'price'=>$row['J'],
              'total_price'=>$row['K'],
            ));
          }
          
          $numrow++; // Tambah 1 setiap kali looping
        }
        // Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
        $this->purchase_order->insert_multiple($data);
        
        redirect("Purchase_Order"); // Redirect ke halaman awal (ke controller Purchase Order fungsi index)
      }

      public function ajax_delete($order_id){
        $this->purchase_order->delete_order($order_id);
        echo json_encode(array("status" => TRUE));
    }
}
